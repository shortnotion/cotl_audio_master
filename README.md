# COTL_AUDIO_MASTER

This repo contains scripts that control the playback of audio en visuals for Children of the Light's Warping Halos installation.

## Devices

The installation consists of the following devices;
* 1x artnet-controlled rotator engine
* 4x artnet-controlled C4 controller that control four full color LED strips (inner/outer/front/back)
* 1x minimad that contains the pre-recorded artnet sequences for the LEDs and the rotator
* 1x raspberrypi 3B that runs the master control scripts (see this repo) and performs audio playback
* 1x Behringer U-PHORIA UMC404D USB Audio Interface, attached to the raspberrypi for 4-channel audio output
* 1x router to which both the minimd and the raspberrypi are connected using an ethernet cable

## Audio Playback

Audio is played using the ```aplay``` command, which uses the ALSA backend.

#### NO PulseAudio
It's NOT using PulseAudio (which seemed to be the default behaviour when invoking the aplay command as a normal user) because the script runs as a systemd service, which for some reason makes it unable to connect to the pulse-audio server (seems to have something to do with having super-user privileges, because using the ```sudo``` when invoking aplay as a normal using causes identical errors).

#### Virtual 20to40 Alsa PCM in ~/.asoundrc
By default, when invoking ```aplay``` as a super-user, the audio interface was only outputting sound from the front left and right audio channels. To clone those channels to the back left and right audio channels (thus outputting sound on all 4 outputs of the audio interface), we created a virtual ALSA PCM, see the ```~/.asoundrc``` on the raspberry or the ```doc/home_dot_asoundrc``` template in this repo.

To use this virtual device the aplay command is invoked with the ```-D plug:20to40``` option (see ```scripts/audio_play.sh```).

#### cfgr.service runs as pi
In order for the configuration in ```/home/pi/.asoundrc``` to have effect, the master control script that runs the aplay command has to be owned by the ```pi``` user and not by ```root``` (which is default for systemd services). See the service file at ```/etc/systemd/system/cfgr.service```. The service is loaded using the ```sudo systemctl enable cfgr``` command.


### [FAILED] Using reaper DAW
Initially we tried performing playback using the reaper DAW, but this proofed too heavy for the raspberry (you could hear clear and disturbing 'cuts' during playback). It also proofed challenging to launch and control Reaper using a startup script when running the raspberrypi in headless mode.
