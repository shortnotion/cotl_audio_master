#!/usr/bin/python

import time
import os, sys

PGREP_PROCESS_NOT_FOUND_EXIT_CODE = 256

# config
if 'test' in sys.argv:
  startup_delay = 0
  stop_minimad_interval = 2
  stop_minimad_count = 0
  start_delay = 0
  show_duration = 5
  pause_between_shows = 2
  iteration_delay = 1
else:
  startup_delay = 5
  stop_minimad_interval = 3
  stop_minimad_count = 5
  start_delay = 10
  show_duration = 1260
  pause_between_shows = 10
  iteration_delay = 1

# give everything some time to startup
time.sleep(startup_delay)

# send multiple stop signals to the minimad
for i in range(stop_minimad_count):
  os.system('./scripts/minimad_stop.sh')
  time.sleep(stop_minimad_interval)

# little break before starting the show interval
time.sleep(start_delay)

# # loop show endlessly at fixed interval with short break in between each iteration
# while True:
#   # play
#   os.system('./scripts/cmd_play.sh')
#   # wait for show to finish
#   time.sleep(show_duration)
#   # stop, both audio and minimad (just to be sure)
#   os.system('./scripts/cmd_stop.sh')
#   # show break
#   time.sleep(pause_between_shows)

# loop endlessly, at each iteration we check if audio is still running, if not, we start a new playback after a short delay
while True:
  # check if there is an aplay process; if audio is (still) playing
  if os.system('pgrep aplay > /dev/null') == PGREP_PROCESS_NOT_FOUND_EXIT_CODE:
    # log
    print('No audio playback detected, starting delay ({}s) for next playback....'.format(pause_between_shows))
    # short delay
    time.sleep(pause_between_shows)
    # start new playback
    os.system('./scripts/cmd_play.sh')

  time.sleep(iteration_delay)


