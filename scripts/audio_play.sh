
# run everything in background so cfgr isn't blocking
# aplay '/home/pi/COTL/WarpingHalosTest01/Harping Halos Sequences_Installation.wav' &>/dev/null &
#aplay '/home/pi/COTL/AlsaStereoTest.wav' &>/dev/null &

# the virtual 20to40 device is defined in ~/.asoundrc
aplay -D plug:20to40 /home/pi/COTL/WarpingHalos.wav &>/dev/null &
#aplay -D plug:40to40 /home/pi/COTL/Alsa1234Test.wav &>/dev/null &
echo "Started playback"
