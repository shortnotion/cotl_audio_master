#!/bin/sh

## These were for debugging
#echo "-- whoami"
#whoami
#echo "-- HOME"
#echo $HOME
#echo "-- ID"
#id
#echo "-- LIST ALSA DEVICES"
#aplay -l
#echo "-- START PLAYBACK"

## This script is run as a service (systemd) which means it's run as root,
## we want to execute the aplay command as user 'pi'
## because it needs to load the alsa configuration from /home/pi/.asoundrc
#su pi -c "aplay -D plug:20to40 /home/pi/COTL/AlsaStereoTest.wav"
#aplay -D plug:20to40 /home/pi/COTL/AlsaStereoTest.wav

#aplay -v -D plughw:CARD=U192k,DEV=0 /home/pi/COTL/AlsaStereoTest.wav

#paplay /home/pi/COTL/AlsaStereoTest.wav

echo "-- cfgr systemd service starting cfgr.app with configuration in /home/pi/COTL/cotl_audio_master --"
cd /home/pi/COTL/cotl_audio_master && /home/pi/COTL/cotl_audio_master/venv/bin/python3 -m cfgr.app
